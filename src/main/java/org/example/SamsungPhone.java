package org.example;

public class SamsungPhone extends Phone implements PhoneMedia, PhoneConnection {
    private boolean sllepingMode;

    public SamsungPhone(int memory, String model, int power, int year, boolean sllepingMode) {
        super();
    }


    public boolean getSllepingMode() {
        return sllepingMode;
    }

    public void setSllepingMode(boolean sllepingMode) {
        this.sllepingMode = sllepingMode;
    }


    @Override
    public void call() {
        System.out.println("Calling...");
    }

    @Override
    public void takecall() {
        System.out.println("take the call...");
    }

    @Override
    public void rejecktcall() {
        System.out.println("to reject call...");
    }

    @Override
    public void vibration() {
        System.out.println("Vibration mode is on");
    }

    @Override
    public void silentMode() {
        System.out.println("Silent mode is mode");
    }

    @Override
    public void dontDistrub() {
        System.out.println("No disturbance mode");
    }

    @Override
    public void photo() {
        System.out.println("make a photo");
    }

    @Override
    public void filming() {
        System.out.println("start to filming");
    }

    @Override
    public void turnOnTheRecorder() {
        System.out.println("Turning on the recorder");
    }

    @Override
    public void nightSceneMode() {
        System.out.println("Night scene mode is on");
    }

    @Override
    public void screenRecording() {
        System.out.println("Screen recording");
    }

 public String toString(){
        return " Memory : " + getMemory() + " Model : " + getModel() + " Power : " + getPower() + " Year : " + getYear()
             + " Sleeping mod is : " + getSllepingMode();

 }
}

