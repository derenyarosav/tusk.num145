package org.example;

public class Main {
    public static void main(String[] args) {
        SamsungPhone samsungPhone = new SamsungPhone(32, "SamsungProMax", 100, 2020,true);
        NokiaPhone nokiaPhone = new NokiaPhone(64, "Nokia3310", 100, 2012,true);
        System.out.println(samsungPhone.toString());
        System.out.println(nokiaPhone.toString());
        samsungPhone.call();
        samsungPhone.silentMode();
        samsungPhone.takecall();
        samsungPhone.vibration();
        samsungPhone.rejecktcall();
        samsungPhone.dontDistrub();
        samsungPhone.filming();
        samsungPhone.photo();
        samsungPhone.screenRecording();
        samsungPhone.nightSceneMode();
        samsungPhone.turnOnTheRecorder();
        nokiaPhone.call();
        nokiaPhone.silentMode();
        nokiaPhone.takecall();
        nokiaPhone.vibration();
        nokiaPhone.rejecktcall();
        nokiaPhone.dontDistrub();
        nokiaPhone.filming();
        nokiaPhone.photo();
        nokiaPhone.screenRecording();
        nokiaPhone.nightSceneMode();
        nokiaPhone.turnOnTheRecorder();

        samsungPhone.setModel("SamsungProMax");
        samsungPhone.setPower(100);
        samsungPhone.setSllepingMode(true);
        samsungPhone.setMemory(64);
        nokiaPhone.setMemory(32);
        nokiaPhone.setYear(2020);
        nokiaPhone.setAntivirusMode(false);
        nokiaPhone.setModel("Nokia3310");
        System.out.println(samsungPhone.toString());
        System.out.println(nokiaPhone.toString());
    }
}