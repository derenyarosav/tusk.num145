package org.example;

public abstract class Phone {
    private int memory;
    private String model;
    private int power;
    private int year;

    public Phone(int memory, String model, int power, int year) {
        this.memory = memory;
        this.model = model;
        this.power = power;
        this.year = year;
    }

    protected Phone() {
    }


    public int getMemory() {
        return memory;
    }

    public void setMemory(int memory) {
        this.memory = memory;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
